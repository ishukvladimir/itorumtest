package com.easysolutions.itorumtest.api;

import com.easysolutions.itorumtest.models.PeopleListContainer;
import com.easysolutions.itorumtest.models.PlanetListContainer;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MessagesApi {
    @GET("/api/planets/")
    Call<PlanetListContainer> getPlanets(
            @Query("format") String format
    );

    @GET("/api/people/")
    Call<PeopleListContainer> getPeople(
            @Query("format") String format
    );

}
