package com.easysolutions.itorumtest.details_fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.easysolutions.itorumtest.R;
import com.easysolutions.itorumtest.models.People;
import com.google.gson.Gson;

public class PeopleDetailsFragment extends Fragment {

    private TextView name;
    private TextView height;
    private TextView mass;
    private TextView hairColor;
    private TextView skinColor;
    private TextView eyeColor;
    private TextView birthYear;
    private TextView gender;
    private TextView homeworld;
    private TextView films;
    private TextView species;
    private TextView vehicles;
    private TextView starships;
    private TextView created;
    private TextView edited;
    private TextView url;

    private People people;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

        if (bundle != null) {
            people = new Gson().fromJson(bundle.getString("people"), People.class);
        }
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_people_details, container, false);

        name = view.findViewById(R.id.name);
        height = view.findViewById(R.id.height);
        mass = view.findViewById(R.id.mass);
        hairColor = view.findViewById(R.id.hairColor);
        skinColor = view.findViewById(R.id.skinColor);
        eyeColor = view.findViewById(R.id.eyeColor);
        birthYear = view.findViewById(R.id.birthYear);
        gender = view.findViewById(R.id.gender);
        homeworld = view.findViewById(R.id.homeworld);
        films = view.findViewById(R.id.films);
        species = view.findViewById(R.id.species);
        vehicles = view.findViewById(R.id.vehicles);
        starships = view.findViewById(R.id.starships);
        created = view.findViewById(R.id.created);
        edited = view.findViewById(R.id.edited);
        url = view.findViewById(R.id.url);

        showFragmentsContent(people);
        return view;
    }

    public void showFragmentsContent(final People people) {

        name.setText(people.getName());
        height.setText(people.getHeight());
        mass.setText(people.getMass());
        hairColor.setText(people.getHairColor());
        skinColor.setText(people.getSkinColor());
        eyeColor.setText(people.getEyeColor());
        birthYear.setText(people.getBirthYear());
        gender.setText(people.getBirthYear());
        homeworld.setText(people.getHomeworld());
        films.setText(getFilmsString(people));
        species.setText(getSpeciesString(people));
        vehicles.setText(getVehiclesString(people));
        starships.setText(getStarshipsString(people));
        created.setText(people.getCreated());
        edited.setText(people.getEdited());
        url.setText(people.getUrl());
    }

    private String getSpeciesString(People people) {
        StringBuilder string = new StringBuilder();
        String n;

        for (int i = 0; i <people.getSpecies().size(); i++){

            if (string.toString().equals(""))
            {
                n = "";
            } else n = "\n";

            string.append(n).append(people.getSpecies().get(i));
        }
        return string.toString();
    }

    private String getFilmsString(People people) {
        StringBuilder string = new StringBuilder();
        String n;

        for (int i = 0; i < people.getFilms().size(); i++){

            if (string.toString().equals(""))
            {
                n = "";
            } else n = "\n";

            string.append(n).append(people.getFilms().get(i));
        }
        return string.toString();
    }

    private String getVehiclesString(People people) {
        StringBuilder string = new StringBuilder();
        String n;

        for (int i = 0; i < people.getVehicles().size(); i++){

            if (string.toString().equals(""))
            {
                n = "";
            } else n = "\n";

            string.append(n).append(people.getVehicles().get(i));
        }
        return string.toString();
    }

    private String getStarshipsString(People people) {
        StringBuilder string = new StringBuilder();
        String n;

        for (int i = 0; i < people.getStarships().size(); i++){

            if (string.toString().equals(""))
            {
                n = "";
            } else n = "\n";

            string.append(n).append(people.getStarships().get(i));
        }
        return string.toString();
    }

}
