package com.easysolutions.itorumtest.details_fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.easysolutions.itorumtest.R;
import com.easysolutions.itorumtest.models.Planet;
import com.google.gson.Gson;

public class PlanetDetailsFragment extends Fragment  {

    private TextView name;
    private TextView rotationPeriod;
    private TextView orbitalPeriod;
    private TextView diameter;
    private TextView climate;
    private TextView gravity;
    private TextView terrain;
    private TextView surfaceWater;
    private TextView population;
    private TextView residents;
    private TextView films;
    private TextView created;
    private TextView edited;
    private TextView url;

    private Planet planet;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            planet = new Gson().fromJson(bundle.getString("planet"), Planet.class);
        }
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_planet_details, container, false);

        name = view.findViewById(R.id.name);
        rotationPeriod = view.findViewById(R.id.rotationPeriod);
        orbitalPeriod = view.findViewById(R.id.orbitalPeriod);
        diameter = view.findViewById(R.id.diameter);
        climate = view.findViewById(R.id.climate);
        gravity = view.findViewById(R.id.gravity);
        terrain = view.findViewById(R.id.terrain);
        surfaceWater = view.findViewById(R.id.surfaceWater);
        population = view.findViewById(R.id.population);
        residents = view.findViewById(R.id.residents);
        films = view.findViewById(R.id.films);
        created = view.findViewById(R.id.created);
        edited = view.findViewById(R.id.edited);
        url = view.findViewById(R.id.url);

        showFragmentsContent(planet);
        return view;
    }

    public void showFragmentsContent(final Planet planet) {

        name.setText(planet.getName());
        rotationPeriod.setText(planet.getRotationPeriod());
        orbitalPeriod.setText(planet.getOrbitalPeriod());
        diameter.setText(planet.getDiameter());
        climate.setText(planet.getDiameter());
        gravity.setText(planet.getGravity());
        terrain.setText(planet.getTerrain());
        surfaceWater.setText(planet.getSurfaceWater());
        population.setText(planet.getPopulation());
        residents.setText(getResidentsString(planet));
        films.setText(getFilmsString(planet));
        created.setText(planet.getCreated());
        edited.setText(planet.getEdited());
        url.setText(planet.getUrl());
    }

    private String getResidentsString(Planet planet) {
        StringBuilder string = new StringBuilder();
        String n;

        for (int i = 0; i <planet.getResidents().size(); i++){

            if (string.toString().equals(""))
            {
                n = "";
            } else n = "\n";

            string.append(n).append(planet.getResidents().get(i));
        }
        return string.toString();
    }

    private String getFilmsString(Planet planet) {
        StringBuilder string = new StringBuilder();
        String n;

        for (int i = 0; i <planet.getFilms().size(); i++){

            if (string.toString().equals(""))
            {
                n = "";
            } else n = "\n";

            string.append(n).append(planet.getFilms().get(i));
        }
        return string.toString();
    }

}
