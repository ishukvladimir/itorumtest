package com.easysolutions.itorumtest.feed_fragment.RecyclerView

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.easysolutions.itorumtest.R
import com.easysolutions.itorumtest.models.People

class PeopleAdapter (val people: ArrayList<People>): RecyclerView.Adapter<ViewHolder>() {

    var peopleInterface: RvPeopleInterface? = null

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvName?.text = people[position].name
        holder.itemView.setOnClickListener { peopleInterface?.onItemPressed(people[position]) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.rv_row, parent, false)
        return ViewHolder(v);
    }

    override fun getItemCount(): Int {
        return people.size
    }
}