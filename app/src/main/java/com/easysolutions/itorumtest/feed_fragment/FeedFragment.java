package com.easysolutions.itorumtest.feed_fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.easysolutions.itorumtest.MainActivity;
import com.easysolutions.itorumtest.R;
import com.easysolutions.itorumtest.feed_fragment.RecyclerView.PeopleAdapter;
import com.easysolutions.itorumtest.feed_fragment.RecyclerView.PlanetsAdapter;
import com.easysolutions.itorumtest.feed_fragment.RecyclerView.RvPeopleInterface;
import com.easysolutions.itorumtest.feed_fragment.RecyclerView.RvPlanetInterface;
import com.easysolutions.itorumtest.models.People;
import com.easysolutions.itorumtest.models.Planet;
import com.easysolutions.itorumtest.repository.GlobalDataLayer;

public class FeedFragment extends Fragment {

    private RecyclerView rvMain;
    private MainActivity mainActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        mainActivity = (MainActivity) getActivity();
        rvMain = view.findViewById(R.id.rvMain);
        rvMain.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    public void  pleaseUpdateMe(){
        switch (MainActivity.getCurrentObjectsType()){

            case MainActivity.PLANET :{
                PlanetsAdapter adapter = new PlanetsAdapter(GlobalDataLayer.getInstance().getPlanetArrayList());
                adapter.setPlanetInterface(new RvPlanetInterface() {
                    @Override
                    public void onItemPressed(Planet planet) {
                        mainActivity.openPlanetDetailsFragment(planet);
                    }
                });
                rvMain.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
            break;

            case MainActivity.PEOPLE: {
                PeopleAdapter adapter = new PeopleAdapter(GlobalDataLayer.getInstance().getPeopleArrayList());
                adapter.setPeopleInterface(new RvPeopleInterface() {
                    @Override
                    public void onItemPressed(People people) {
                        mainActivity.openPeopleDetailsFragment(people);
                    }
                });
                rvMain.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
            break;

        }
    }

}