package com.easysolutions.itorumtest.feed_fragment.RecyclerView;

import com.easysolutions.itorumtest.models.People;

public interface RvPeopleInterface {
    void onItemPressed(People people);
}
