package com.easysolutions.itorumtest.feed_fragment.RecyclerView;

import com.easysolutions.itorumtest.models.Planet;

public interface RvPlanetInterface {
    void onItemPressed(Planet planet);
}
