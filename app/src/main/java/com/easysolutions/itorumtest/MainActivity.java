package com.easysolutions.itorumtest;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.util.ArrayList;

import com.easysolutions.itorumtest.details_fragments.PeopleDetailsFragment;
import com.easysolutions.itorumtest.models.People;
import com.easysolutions.itorumtest.models.PeopleListContainer;
import com.easysolutions.itorumtest.feed_fragment.FeedFragment;
import com.easysolutions.itorumtest.details_fragments.PlanetDetailsFragment;
import com.easysolutions.itorumtest.models.Planet;
import com.easysolutions.itorumtest.models.PlanetListContainer;
import com.easysolutions.itorumtest.repository.GlobalDataLayer;
import com.google.gson.Gson;
import org.jetbrains.annotations.NotNull;

public class MainActivity extends AppCompatActivity {

    private FragmentManager fragmentManager;
    private RelativeLayout rlBackButton;
    private TextView tvAppName;

    public static final int PLANET = 1;
    public static final int PEOPLE = 2;
    private static int currentObjectsType = PLANET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rlBackButton = findViewById(R.id.rlBackButton);
        tvAppName = findViewById(R.id.tvAppName);

        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Fragment currentFragment = fragmentManager.findFragmentById(R.id.rlContainer);
                if (currentFragment == null) finish();
                if (currentFragment instanceof FeedFragment) {
                    // логика этого фрагмента перед открытием
                    updateFeedFragment();
                    backButtonGone();
                }
                if (currentFragment instanceof PlanetDetailsFragment){
                    // логика этого фрагмента перед открытием
                }
            }
        });

        rlBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFeedFragment();
            }
        });
        getPlanetFromServer();
        openFeedFragment();
    }

    private void backButtonGone() {
        rlBackButton.setVisibility(View.GONE);
        if (currentObjectsType == PEOPLE) {
            tvAppName.setText(getString(R.string.people));
        } else {
            tvAppName.setText(getString(R.string.planet));
        }
    }

    public void getPlanetFromServer() {
        Call<PlanetListContainer> message = App.getApi().getPlanets("json");
        message.enqueue(new Callback<PlanetListContainer>() {

            @Override
            public synchronized void onResponse(@NotNull Call<PlanetListContainer> call, @NotNull Response<PlanetListContainer> response) {
                PlanetListContainer feed = response.body();

                // записываем полученный ArrayList в репозиторий
                if (feed != null) {
                    GlobalDataLayer.getInstance().setPlanetArrayList((ArrayList<Planet>) feed.getResults());
                }

                getPeopleFromServer();
            }

            @Override
            public synchronized void onFailure(@NotNull Call<PlanetListContainer> call, @NotNull Throwable t) {
                showToast("Не могу получить данные с сервера");
            }
        });
    }

    public void getPeopleFromServer() {
        Call<PeopleListContainer> message = App.getApi().getPeople("json");
        message.enqueue(new Callback<PeopleListContainer>() {

            @Override
            public synchronized void onResponse(@NotNull Call<PeopleListContainer> call, @NotNull Response<PeopleListContainer> response) {
                PeopleListContainer feed = response.body();

                // записываем полученный ArrayList в репозиторий
                if (feed != null) {
                    GlobalDataLayer.getInstance().setPeopleArrayList((ArrayList<People>) feed.getResults());
                }

                updateFeedFragment();
            }

            @Override
            public synchronized void onFailure(@NotNull Call<PeopleListContainer> call, @NotNull Throwable t) {
                showToast("Не могу получить данные с сервера");
            }
        });
    }

    private void updateFeedFragment() {
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.rlContainer);
        if (currentFragment instanceof FeedFragment) {
            ((FeedFragment) currentFragment).pleaseUpdateMe();
        }
    }

    public void openFeedFragment() {
        backButtonGone();

        Fragment fragment = new FeedFragment();
        fragmentManager
                .beginTransaction()
                .replace(R.id.rlContainer, fragment)
                .addToBackStack(null)
                .commit();

        //requestAndShow();
        updateFeedFragment();
    }

    public void openPlanetDetailsFragment(Planet planet) {

        backButtonVisible();

        Fragment fragment = new PlanetDetailsFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable("planet", new Gson().toJson(planet));
        fragment.setArguments(bundle);

        fragmentManager.beginTransaction().replace(R.id.rlContainer, fragment).addToBackStack(null).commit();
    }

    private void backButtonVisible() {
        rlBackButton.setVisibility(View.VISIBLE);
    }

    public void openPeopleDetailsFragment(People people) {

        backButtonVisible();

        Fragment fragment = new PeopleDetailsFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable("people", new Gson().toJson(people));
        fragment.setArguments(bundle);

        fragmentManager.beginTransaction().replace(R.id.rlContainer, fragment).addToBackStack(null).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {

                case R.id.planet:
                    currentObjectsType = PLANET;
                    openFeedFragment();
                    return true;

                case R.id.people:
                    currentObjectsType = PEOPLE;
                    openFeedFragment();
                    return true;

            }
            return false;
        }
    };

    public void showToast(String string) {
        Toast toast = Toast.makeText(getApplicationContext(),
                string,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static int getCurrentObjectsType() {
        return currentObjectsType;
    }

}
