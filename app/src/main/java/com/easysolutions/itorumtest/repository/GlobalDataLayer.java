package com.easysolutions.itorumtest.repository;

import java.util.ArrayList;

import com.easysolutions.itorumtest.models.People;
import com.easysolutions.itorumtest.models.Planet;

public class GlobalDataLayer {

    private static GlobalDataLayer instance;

    private ArrayList<Planet> planet = new ArrayList<>();
    private ArrayList<People> people = new ArrayList<>();

    public static synchronized GlobalDataLayer getInstance() {
        if (instance == null) {
            instance = new GlobalDataLayer();
        }
        return instance;
    }
    private GlobalDataLayer(){ }

    public ArrayList<Planet> getPlanetArrayList() {
        return planet;
    }

    public ArrayList<People> getPeopleArrayList() {
        return people;
    }

    public void setPlanetArrayList(ArrayList<Planet> planet) {

        this.planet.clear();
        this.planet.addAll(planet);
    }

    public void setPeopleArrayList(ArrayList<People> people) {

        this.people.clear();
        this.people.addAll(people);
    }
}
