package com.easysolutions.itorumtest;

import android.app.Application;

import com.easysolutions.itorumtest.api.MessagesApi;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {

    private static MessagesApi messagesApi;

    @Override
    public void onCreate() {
        super.onCreate();
        initRetrofit();
    }

    private void initRetrofit(){
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://swapi.co")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        messagesApi = retrofit.create(MessagesApi.class);
    }

    public static MessagesApi getApi(){
        return messagesApi;
    }
}
